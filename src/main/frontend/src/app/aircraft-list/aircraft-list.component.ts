import { Component, OnInit } from '@angular/core';
import { AircraftService } from '../shared/aircraft/aircraft.service';
import { GiphyService } from '../shared/giphy/giphy.service';

@Component({
  selector: 'app-aircraft-list',
  templateUrl: './aircraft-list.component.html',
  styleUrls: ['./aircraft-list.component.css']
})
export class AircraftListComponent implements OnInit {
  longDistanceAircrafts: Array<any>;
  shortDistanceAircrafts: Array<any>;
  otherAircrafts: Array<any>;
  constructor(private aircraftService:AircraftService, private giphyService: GiphyService) { }

  ngOnInit() {
    this.aircraftService.getLongDistanceAircrafts().subscribe(data => {
      this.longDistanceAircrafts = data;
      for (const aircraft of this.longDistanceAircrafts) {
        this.giphyService.get(aircraft.aircraftRegistration).subscribe(url => aircraft.giphyUrl = url);
      }
    });
    this.aircraftService.getShortDistanceAircrafts().subscribe(data => {
      this.shortDistanceAircrafts = data;
      for (const aircraft of this.shortDistanceAircrafts) {
        this.giphyService.get(aircraft.aircraftRegistration).subscribe(url => aircraft.giphyUrl = url);
      }
    });
    this.aircraftService.getOtherAircrafts().subscribe(data => {
      this.otherAircrafts = data;
      for (const aircraft of this.otherAircrafts) {
        this.giphyService.get(aircraft.aircraftRegistration).subscribe(url => aircraft.giphyUrl = url);
      }
    });
  }

}
