package de.lhind.mro.springofficedayexample.repository;

import de.lhind.mro.springofficedayexample.entity.Aircraft;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface AircraftRepository extends JpaRepository<Aircraft, Long> {
}