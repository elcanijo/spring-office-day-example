package de.lhind.mro.springofficedayexample.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Aircraft {
    @Id
    @GeneratedValue
    private Long id;
    @NonNull
    private String aircraftRegistration;
    @NonNull
    private String aircraftType;

    private String giphyUrl;

}
