package de.lhind.mro.springofficedayexample.rest;

import de.lhind.mro.springofficedayexample.entity.Aircraft;
import de.lhind.mro.springofficedayexample.repository.AircraftRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
class AircraftController {
    private AircraftRepository repository;

    public AircraftController(AircraftRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/long-distance-aircrafts")
    @CrossOrigin(origins = "http://localhost:4200")
    public Collection<Aircraft> getLongDistanceAircrafts() {
        return repository.findAll().stream()
                .filter(this::isLongDistance)
                .collect(Collectors.toList());
    }

    @GetMapping("/short-distance-aircrafts")
    @CrossOrigin(origins = "http://localhost:4200")
    public Collection<Aircraft> getShortDistanceAircrafts() {
        return repository.findAll().stream()
                         .filter(this::isShortDistance)
                         .collect(Collectors.toList());
    }

    @GetMapping("/other-aircrafts")
    @CrossOrigin(origins = "http://localhost:4200")
    public Collection<Aircraft> getOtherAircrafts() {
        return repository.findAll().stream()
                         .filter(this::isOther)
                         .collect(Collectors.toList());
    }

    private boolean isLongDistance(Aircraft aircraft) {
        return aircraft.getAircraftType() != null && "B747,A350,A380".contains(aircraft.getAircraftType());
    }

    private boolean isShortDistance(Aircraft aircraft) {
        return aircraft.getAircraftType() != null && "A320,A320Neo".contains(aircraft.getAircraftType());
    }

    private boolean isOther(Aircraft aircraft) {
        return !isLongDistance(aircraft) && !isShortDistance(aircraft);
    }
}