package de.lhind.mro.springofficedayexample;

import de.lhind.mro.springofficedayexample.entity.Aircraft;
import de.lhind.mro.springofficedayexample.repository.AircraftRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.stream.Stream;

@SpringBootApplication
public class SpringOfficeDayExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringOfficeDayExampleApplication.class, args);
    }

    @Bean
    ApplicationRunner init(AircraftRepository repository) {
        return args -> {
            Stream.of(
                Arrays.asList("D-ABYA", "B747", "https://media.giphy.com/media/yLPIupXMTIz8Q/giphy.gif"),
                Arrays.asList("D-AIPA", "A320", "https://media.giphy.com/media/w07NKhoRR2suI/giphy.gif"),
                Arrays.asList("D-AINA", "A320Neo", "http://imgcdn.airliners.de/2018/07/neo_5da71f2a3a7f8f9199b616d05fd35f45_rb_597.jpg"),
                Arrays.asList("D-AIMA", "A380", "https://media.giphy.com/media/ekSh8Qp2noLeM/giphy.gif"),
                Arrays.asList("D-AIXA", "A350", "https://media.giphy.com/media/9ZWmd9yKt0P4I/giphy.gif")).forEach(list -> {
                Aircraft aircraft = new Aircraft();
                aircraft.setAircraftRegistration(list.get(0));
                aircraft.setAircraftType(list.get(1));
                aircraft.setGiphyUrl(list.get(2));
                repository.save(aircraft);
            });
            repository.findAll().forEach(System.out::println);
        };
    }
}
